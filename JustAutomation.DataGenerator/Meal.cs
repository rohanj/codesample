﻿using System;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustAutomation.DataGenerator
{
    // currently unused
    public class Meal
    {
        // I originally built this as I was passing meal objects into pages
        // However I decided a random approach would be more pragmatic due to 
        // there being different restaurants available at different times
        // of the day. As a consequence, the random approach is probably
        // able to hit a wider range of functionality

        public readonly string Restaurant;
        public readonly ImmutableDictionary<string, ImmutableList<string>> MealsWithOptions;

        public Meal()
        {
            Restaurant = "Demo-Just Eat";
            var options =  ImmutableList.Create("Sweet and Sour Chicken","Noodles");
            MealsWithOptions = ImmutableDictionary.Create<string, ImmutableList<string>>()
                                                  .Add("Set Meal A", options);
        }
        
        public static Meal Get(string restaurant)
        {
            return new Meal();
        }

        public override string ToString()
        {
            var output = new StringBuilder();
            output.Append("\n\t\t")
                  .Append("| ")
                  .Append(Restaurant)
                  .Append(" | ");

            foreach(var meal in MealsWithOptions)
            {
                output.Append(meal.Key)
                      .Append(" | " );  
                meal.Value.ForEach( option => output.Append(option)
                                                    .Append( ","));
                output.Remove(output.Length-1, 1)
                      .Append(" |")
                      .Append('\n');
            }

            return output.ToString();
        }
    }
}
