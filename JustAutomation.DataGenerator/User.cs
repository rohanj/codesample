﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace JustAutomation.DataGenerator
{
    public class Address
    {
        public readonly string Phone;
        public readonly string Address1;
        public readonly string City;

        internal Address(string phone, string address1, string city)
        {
            Phone = phone;
            Address1 = address1;
            City = city;
        }

        public override string ToString()
        {
            return String.Format("address1: {0}, city: {1}, phone: {2}", Address1, City, Phone);
        }
    }

    public class SignUpDetails
    {

        public readonly string Name;
        public readonly string Email;
        public readonly string Password;

        internal SignUpDetails(string name, string email, string password)
        {
            Name = name;
            Email = email;
            Password = password;
        }

        public override string ToString()
        {
            return String.Format("name: {0}, email: {1}, password: {2}", Name, Email, Password);
        }
    }

    public class User
    {
        public readonly SignUpDetails SignUpDetails;
        public readonly Address Address;

        // Would be better to use the builder pattern instead of what may become a very long 
        //constructor
        User(string name, string email, string password, string address1, string city, string phone)
        {
            this.SignUpDetails = new SignUpDetails(name,email,password);
            this.Address = new Address(phone, address1, city);
        }

        public static User GetNew()
        {
            var surname = RandomAlphabeticString();
            var name = "Test " + surname;
            var email = "test.automation.box+" + surname + "@gmail.com";
            var password = "automation";
            var phone = "07123456789";
            var address1 = "12 Test Drive";
            var city = "Auto May Town";
            return new User(name, email, password, address1, city, phone );
        }

        public static string RandomAlphabeticString()
        {
            var name = Path.GetRandomFileName().Replace(".", "");
            name = Regex.Replace(name, @"\d", "");
            return name;
        }
    }
}
