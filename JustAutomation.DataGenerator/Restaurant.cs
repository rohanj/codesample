﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustAutomation.DataGenerator
{
    // currently unused
    // left it in here to show my thought process
    // a better approach to choose restaurants would be to do a query on the DB that holds 
    // the test restaurant data, and choose one of those
    public class Restaurant
    {
        // If I had a full list of restaurants I would have included them all
        public static Dictionary<string, List<string>> Restaurants = new Dictionary<string, List<string>>()
        {
            {
                "AR51 1AA", new List<string>()
                {
                    "Demo-JUST EAT","DANIEL RODGERS DEMO","Kieran - Orderpad - Test Restaurant"
                }
            }
        };

        public static List<string> GetExpected(string area)
        {
            return Restaurants[area];
        }
    }
}
