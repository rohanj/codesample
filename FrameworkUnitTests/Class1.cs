﻿using JustAutomation.DataGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xunit;

namespace FrameworkUnitTests
{
    public class UserDataGeneratorTests
    {
        // I used this project to aid in development of the framework. At the moment 
        // it contains no proper tests, but setting up a unit testing framework
        // allows me to tinker with methods to make them correct without the overhead
        // of running the method in a UI test.
         
        [Fact]
        public void RandomAlphabetStringTest()
        {
            for (int i = 0; i < 100; i++)
            {
                var randomString = User.RandomAlphabeticString();
                var nextString = Regex.Replace(randomString,@"\d", "");
            }
        }
    }
}
