﻿using JustAutomation.Pages.Web;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JustAutomation.Pages
{
    public class PageDriverContext
    {
        private IWebDriver Driver;

        public IWebDriver GetDriver()
        {
            if (Driver == null)
                throw new NullReferenceException("PageDriver.Driver is null");
            return Driver; 
        }

        public PageDriverContext()
        {
            Driver = new FirefoxDriver();
        }

        public void Open()
        {
            Driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromMilliseconds(1));
            Driver.Manage().Timeouts().SetPageLoadTimeout(TimeSpan.FromSeconds(10));
            Driver.Manage().Window.Maximize();
            Driver.Navigate().GoToUrl("http://www.just-eat.co.uk");
        }


        public void Quit()
        {
            Driver.Quit();
        }

        public async Task<Type> CurrentPage()
        {
            var pageTypes = new List<Type>();
            var tasks = new List<Task>();
            tasks.Add( GetResponseFromPageAsync(() => new DeliveryDetailsPage(this), pageTypes));
            tasks.Add( GetResponseFromPageAsync(() => new SignUpPage(this), pageTypes));

            await Task.WhenAny(tasks);

            var pageType = pageTypes.First();
            return pageType;
        }

        private Task GetResponseFromPageAsync<TPage>(Func<TPage> pageRequest, List<Type> pageTypes) where TPage : PageBase
        {
            return Task.Factory.StartNew(() => pageTypes.Add(GetResponseFromPage(pageRequest)));
        }

        private Type GetResponseFromPage<TPage>(Func<TPage> pageRequest) where TPage : PageBase
        {
            Type page = typeof(PageBase);
            try
            {
                page = pageRequest().GetType(); // throws PageTimeOutException if it doesn't exist
            }
            catch (Exception) { }

            return page;

        }

    }
}
