﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustAutomation.Pages.Web
{
    public class SiteHeader : PageBase
    {
        [FindsBy(How = How.CssSelector, Using = ".login")]
        IWebElement LogInButton;

        public SiteHeader(PageDriverContext context) : base(context)
        {
            PageFactory.InitElements(Driver, this);
        }

        public SignInPage GoToSignInPage()
        {
            LogInButton.Click();
            return new SignInPage(Context);
        }

    }
}
