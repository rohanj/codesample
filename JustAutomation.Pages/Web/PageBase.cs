﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace JustAutomation.Pages.Web
{
    public abstract class PageBase 
    {
        protected IWebDriver Driver;
        protected WebDriverWait Wait;
        protected PageDriverContext Context;
        public static readonly TimeSpan PageLoadTimeout = TimeSpan.FromSeconds(5);


        protected PageBase(PageDriverContext context)
        {
            Context = context;
            Driver = Context.GetDriver();
            Wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            Wait.PollingInterval = TimeSpan.FromMilliseconds(50);
        }
        
    }
}
