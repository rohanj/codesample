﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;
using JustAutomation.Pages.Extensions;
namespace JustAutomation.Pages.Web
{
    public class Homepage : PageBase
    {
        [FindsBy(How = How.Id, Using = "where")]
        IWebElement LocationField;

        [FindsBy(How = How.Id, Using = "btnSearch")]
        IWebElement SearchButton;

        public Homepage(PageDriverContext context) : base(context)
        {
            PageFactory.InitElements(Driver, this);
        }

        public void Search(string area)
        {
            LocationField.EnterText(area)
                         .SendReturnKey();

        }
        
    }
}
