﻿using JustAutomation.Pages.Extensions;
using JustAutomation.Pages.Web;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace JustAutomation.Pages.Web
{
    public class SearchResultsPage : PageBase
    {
        [FindsBy(How = How.CssSelector, Using = ".viewMenu")]
        IList<IWebElement> OnlineRestaurants;

        [FindsBy(How = How.CssSelector, Using = ".name")]
        IList<IWebElement> RestaurantNames;


        public SearchResultsPage(PageDriverContext context) : base(context)
        {
            PageFactory.InitElements(Driver, this);
        }

        public void ChooseARestaurant()
        {
            // Due to different restaurants being available at different times, and not possessing the
            // data that defines this, I thought the best approach was to choose a random open restaurant
            var restaurant = OnlineRestaurants.GetRandom();
            restaurant.Click();
        }

        public ImmutableList<string> GetRestaurantNames()
        {
            var listBuilder = ImmutableList.CreateBuilder<string>();
          
            foreach(var restaurant in RestaurantNames.Select( rn => rn.Text))
            {
                listBuilder.Add(restaurant);
            }
            return listBuilder.ToImmutableList();
        }
    }
}
