﻿using JustAutomation.Pages.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustAutomation.Pages.Web
{
    public class OrderPaymentPage : PageBase
    {
        [FindsBy(How = How.CssSelector, Using = ".activator[href = \"#Cash-Option\"]")]
        IWebElement PayWithCash;

        [FindsBy(How = How.CssSelector, Using = "#placeOrder")]
        IWebElement PlaceOrderButton;

        public OrderPaymentPage(PageDriverContext context) : base(context)
        {
            PageFactory.InitElements(Driver, this);
            Wait.Until(driver => PayWithCash.Displayed, PageLoadTimeout);
        }

        public OrderPaymentPage SelectPayWithCash()
        {
            PayWithCash.Click();
            return this;
        }

        public void PlaceOrder()
        {
            PlaceOrderButton.Click();
        }
    }
}
