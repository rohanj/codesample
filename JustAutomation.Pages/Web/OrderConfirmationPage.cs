﻿using JustAutomation.Pages.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustAutomation.Pages.Web
{
    public class OrderConfirmationPage : PageBase
    {

        [FindsBy(How = How.CssSelector, Using = ".orderConfirmation")]
        IWebElement OrderConfirmationMessage;

        public OrderConfirmationPage(PageDriverContext context) : base(context)
        {
            PageFactory.InitElements(Driver, this);
            Wait.Until(driver => OrderConfirmationMessage.Displayed, PageLoadTimeout);
        }
    }
}
