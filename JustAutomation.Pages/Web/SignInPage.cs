﻿using JustAutomation.DataGenerator;
using JustAutomation.Pages.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustAutomation.Pages.Web
{
    public class SignInPage : PageBase
    {

        public SignInPage(PageDriverContext context) : base(context)
        {
            PageFactory.InitElements(Driver, this);
            Wait.Until((driver) => SignInButton.Displayed, PageLoadTimeout);
        }

        [FindsBy(How = How.CssSelector, Using = ".recaptcha-checkbox-checkmark")]
        IWebElement RecaptchaCheckbox;

        [FindsBy(How = How.CssSelector, Using = "#Email")]
        IWebElement EmailField;

        [FindsBy(How = How.CssSelector, Using = "#Password")]
        IWebElement PasswordField;

        [FindsBy(How = How.CssSelector, Using = ".submit")]
        IWebElement SignInButton;

        [FindsBy(How = How.CssSelector, Using = ".signup>a")]
        IWebElement SignUpLink;

        public SignUpPage GoToSignUpPage()
        {
            SignUpLink.Click();
            return new SignUpPage(Context);
        }

        public void SignIn(SignUpDetails user)
        {
            EmailField.EnterText(user.Email);
            PasswordField.EnterText(user.Password);
            RecaptchaCheckbox.Click();
            SignInButton.Click();
        }
    }
}
