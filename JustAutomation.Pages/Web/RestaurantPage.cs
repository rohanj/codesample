﻿using JustAutomation.Pages.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustAutomation.Pages.Web
{
    public class RestaurantPage : PageBase
    {
        [FindsBy(How = How.CssSelector, Using = ".checkoutButton")]
        IWebElement Checkout;

        [FindsBy(How = How.CssSelector, Using = "#customisableProductDialog")]
        IWebElement CustomProductDialog;

        [FindsBy(How = How.CssSelector, Using = ".product .addButton")]
        IList<IWebElement> Products;


        // Use better selectors for the 2 below
        readonly By BySkipExtrasSelector = By.CssSelector("[value =\"Skip Extras\"]");
        readonly By ByAddToBasketSelector = By.CssSelector("[value =\"Add to Basket\"]");

        By ByCustomProductOptionSelector = By.CssSelector(".control-label");

        public RestaurantPage(PageDriverContext context) : base(context)
        {
            PageFactory.InitElements(Driver, this);
        }

        public RestaurantPage CreateAnOrder()
        {
            while (!Checkout.Enabled)
            {
                var product = Products.GetRandom();
                AddMeal(product);
            }
            return this;
        }

        public void GoToCheckout()
        {
            Checkout.Click();
        }

        public void AddMeal(IWebElement product)
        {
            Wait.Until(() => product.TryClick(), TimeSpan.FromSeconds(5));
           
            while (DialogIsOpen())
            {
                if (TryClickSkipExtras())
                    continue;

                if (TryAddToBasket())
                    break;

                SelectRandomOption();
            }
        }

        private bool DialogIsOpen()
        {
            return Wait.TryUntil(driver => 
                                CustomProductDialog.Displayed, TimeSpan.FromSeconds(5)
                        );
        }

        private void SelectRandomOption()
        {
            var option = CustomProductDialog.FindElements(ByCustomProductOptionSelector);
            option.GetRandom().Click();
        }

        private bool TryAddToBasket()
        {
            IWebElement tempElement = null;
            if (Driver.TryFindElement(ByAddToBasketSelector, ref tempElement))
            {
                tempElement.Click();
                Wait.Until(driver => !CustomProductDialog.Displayed, TimeSpan.FromSeconds(5));
                return true;
            }
            return false;
        }

        private bool TryClickSkipExtras()
        {
            IWebElement tempElement = null;
            if (Driver.TryFindElement(BySkipExtrasSelector, ref tempElement))
            {
                tempElement.Click();
                Wait.TryUntil(driver => !tempElement.Displayed, TimeSpan.FromSeconds(5));
                return true;
            }
            return false;
        }
    }
}
