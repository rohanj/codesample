﻿using JustAutomation.Pages.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustAutomation.Pages.Web
{
    public class DeliveryTimeConfirmationPage : PageBase
    {
        [FindsBy(How = How.CssSelector, Using = ".submit")]
        IWebElement SubmitButton;

        [FindsBy(How = How.CssSelector, Using = "#confirmOrderDetails")]
        IWebElement Form;

        public DeliveryTimeConfirmationPage(PageDriverContext context) : base(context)
        {
            PageFactory.InitElements(Driver, this);
            Wait.Until(driver => Form.Displayed, PageBase.PageLoadTimeout);
        }

        public void Submit()
        {
            SubmitButton.Click();
        }
    }
}
