﻿using JustAutomation.Pages.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustAutomation.Pages.Web
{
    public class DeliveryDetailsPage : PageBase
    {
        [FindsBy(How= How.CssSelector, Using = "#deliveryDetailsForm")]
        IWebElement DeliveryForm;

        [FindsBy(How = How.CssSelector, Using = "#Phone")]
        IWebElement PhoneField;

        [FindsBy(How = How.CssSelector, Using = "#Address_Line1")]
        IWebElement Address1Field;

        [FindsBy(How = How.CssSelector, Using = "#Address_City")]
        IWebElement CityField;

        [FindsBy(How = How.CssSelector, Using = ".submit")]
        IWebElement SubmitButton;

        public DeliveryDetailsPage(PageDriverContext context) : base(context)
        {
            PageFactory.InitElements(Driver, this);
            Wait.Until(driver => DeliveryForm.Displayed);
        }
        
        public DeliveryDetailsPage Phone(string number)
        {
            PhoneField.EnterText(number);
            return this;
        }

        public DeliveryDetailsPage Address1(string address)
        {
            Address1Field.EnterText(address);
            return this;
        }

        public DeliveryDetailsPage City(string address)
        {
            CityField.EnterText(address);
            return this;
        }

        public void Submit()
        {
            SubmitButton.Click();
        }
    }
}
