﻿using JustAutomation.DataGenerator;
using JustAutomation.Pages.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace JustAutomation.Pages.Web
{
    public class SignUpPage : PageBase 
    {

        public SignUpPage(PageDriverContext context) : base(context)
        {
            PageFactory.InitElements(Driver, this);
            Wait.Until(driver => NameField.Displayed);
        }
        [FindsBy(How = How.CssSelector, Using = "#Name")]
        IWebElement NameField;

        [FindsBy(How = How.CssSelector, Using = "#Email")]
        IWebElement EmailField;

        [FindsBy(How = How.CssSelector, Using = "#Password")]
        IWebElement PasswordField;

        [FindsBy(How = How.CssSelector, Using = ".submit")]
        IWebElement SubmitButton;

        [FindsBy(How = How.CssSelector, Using = ".errorSummary")]
        IWebElement ErrorSummary;

        //return type here is void as I'm not quite sure if the trade off between
        // making this method fluent and dealing with the complexity to return 
        // different types of pages is worth it in the long term.
        public void SignUp(SignUpDetails user)
        {
            NameField.EnterText(user.Name);
            EmailField.EnterText(user.Email);
            PasswordField.EnterText(user.Password);
            SubmitButton.Click();
        }

        public string GetErrorSummary()
        {
            return ErrorSummary.Text;
        }

    }
}
