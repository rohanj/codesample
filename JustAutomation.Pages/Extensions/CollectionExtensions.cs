﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustAutomation.Pages.Extensions
{
    public static class CollectionExtensions
    {
        public static T GetRandom<T>(this IEnumerable<T> e)
        {
            var size = e.Count();
            var randomIndex = new Random().Next(0, size);
            return e.ElementAt(randomIndex);
        }
    }
}
