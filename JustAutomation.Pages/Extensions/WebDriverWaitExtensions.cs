﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JustAutomation.Pages.Extensions
{
    public static class WebDriverWaitExtensions
    {
        public static bool TryUntil(this WebDriverWait wait, Func<IWebDriver, bool> waitCondx, TimeSpan timeout)
        {
            try
            {
                return wait.Until(waitCondx, timeout);
            }
            catch(Exception)
            {
                return false;
            }
        }

        public static bool Until(this WebDriverWait wait, Func<IWebDriver, bool> waitCondx, TimeSpan timeout )
        {
            var defaultTimeout = wait.Timeout;
            wait.Timeout = timeout;
            try
            {
                wait.Until(waitCondx);
                wait.Timeout = defaultTimeout;
                return true;
            }
            catch(Exception e)
            {
                wait.Timeout = defaultTimeout;
                throw e;
            }
        }

        // this could be in a Wait class of its own, since it isn't using any of the webdriver methods
        // but since the only use case is for waiting for elements to do some action, and this framework
        // will never actually be extended for real use, this is fine for now.
        public static bool Until(this WebDriverWait wait, Func<bool> waitCondx, TimeSpan timeout)
        {
            try
            {
                var waitTime = TimeSpan.FromTicks(DateTime.Now.Ticks + timeout.Ticks).Ticks;
                while(ConditionNotCompleteAndNotTimedOut(waitCondx, waitTime))
                {
                    Thread.Sleep(50);
                }
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static bool ConditionNotCompleteAndNotTimedOut(Func<bool> condx, long timeout)
        {
            var condxComplete = condx();
            var timedOut = timeout < DateTime.Now.Ticks;
            return !condxComplete && !timedOut;
        }
    }
}
