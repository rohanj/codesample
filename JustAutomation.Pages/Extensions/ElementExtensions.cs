﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace JustAutomation.Pages.Extensions
{
    public static class ElementExtensions
    {
        public static IWebElement EnterText(this IWebElement e, string text)
        {
            e.SendKeys(text);
            return e;
        }

        public static IWebElement SendReturnKey(this IWebElement e) {
            e.SendKeys(Keys.Return);
            return e;
        }

        public static bool TryFindElement(this ISearchContext e, By by,ref IWebElement innerElement)
        {
            try
            {
                innerElement = e.FindElement(by);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public static bool LocationStabilised(this IWebElement e)
        {
            var initialLocation = e.Location;
            Thread.Sleep(50);
            var finalLocation = e.Location;
            return initialLocation == finalLocation;
        }

        public static bool TryClick(this IWebElement e)
        {
            try
            {
                e.Click();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
