﻿using System;
using TestStack.BDDfy;
using JustAutomation.DataGenerator;
using JustAutomation.Pages;
using Xunit;
using Shouldly;
using JustAutomation.Pages.Web;
using System.Threading.Tasks;

namespace JustAutomation
{
    [Story(
       AsA = "As a Restaurant Owner",
       IWant = "I want to make sure that only humans are ordering food",
       SoThat = "So that I only make food for real customers")]
    public class WebsiteIsSafeFromAutomationBots : CommonSteps
    {
        [Fact]
        public void PreventBotsFromCreatingAccounts()
        {
            var user = User.GetNew();
            var area = "AR51 1PL";
            this.Given(step => _I_have_made_an_order_for_the_meal(area), includeInputsInStepTitle: false)
                .When(step => _I_create_an_account_with(user.SignUpDetails))
                .Then( step => _I_am_required_to_prove_I_am_human() )
                .BDDfy();
        }

        [StepTitle(" I am required to prove that I am human: ")]
        private async void _I_am_required_to_prove_I_am_human()
        {
            var pageType = await JustEatContext.CurrentPage();
            pageType.ShouldBe( typeof(SignUpPage)); 
            
            // if page is correct make sure error is correct
            // don't assert on full error text, may be subject to changes
            _signUpPage.GetErrorSummary().ToLower().ShouldContain("recaptcha");
        }
    }
}
