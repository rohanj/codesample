﻿using JustAutomation.Pages;
using JustAutomation.Pages.Web;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestStack.BDDfy.Configuration;
using TestStack.BDDfy.Reporters.Html;

namespace JustAutomation
{
    public class TestBase : IDisposable
    {
        protected PageDriverContext JustEatContext;

        public TestBase()
        {
            JustEatContext = new PageDriverContext();
            JustEatContext.Open();
            ConfigureReportSettings();
        }

        private void ConfigureReportSettings()
        {
            
            Configurator.BatchProcessors.Add(new HtmlReporter(
                new JustEatReportConfig(), new MetroReportBuilder()));
            var report = Configurator.BatchProcessors.HtmlMetroReport;
            Configurator.BatchProcessors.DiagnosticsReport.Enable();
            Configurator.BatchProcessors.MarkDownReport.Enable();
        }

        public void Dispose()
        {
            JustEatContext.Quit();
        }

        protected Homepage _homepage {
            get { return new Homepage(JustEatContext); }
        }

        protected SearchResultsPage _searchResultsPage
        {
            get { return new SearchResultsPage(JustEatContext); }
        }

        protected RestaurantPage _restaurantPage
        {
            get { return new RestaurantPage(JustEatContext); }
        }

        protected SignInPage _signInPage
        {
            get { return new SignInPage(JustEatContext); }
        }

        protected SignUpPage _signUpPage
        {
            get { return new SignUpPage(JustEatContext); }
        }

        protected DeliveryDetailsPage _deliveryDetailsPage
        {
            get{ return new DeliveryDetailsPage(JustEatContext); }
        }

        protected DeliveryTimeConfirmationPage _deliveryTimeConfirmationPage
        {
            get { return new DeliveryTimeConfirmationPage(JustEatContext); }
        }

        protected OrderPaymentPage _orderPaymentPage
        {
            get { return new OrderPaymentPage(JustEatContext); }
        }

        protected OrderConfirmationPage _orderConfirmationPage
        {
            get { return new OrderConfirmationPage(JustEatContext); }
        }

        protected SiteHeader _siteHeader
        {
            get { return new SiteHeader(JustEatContext); }
        }

    }
}
