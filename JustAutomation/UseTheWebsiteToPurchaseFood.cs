﻿using JustAutomation.DataGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.BDDfy;
using Xunit;
using Shouldly;

namespace JustAutomation
{
    enum PayMethod { Cash,Card}

    [Story(
    AsA = "As a hungry customer",
    IWant = "I want to order some nourishing food",
    SoThat = "So that I can satisfy my hunger")]
    public class UseTheWebsiteToPurchaseFood : CommonSteps
    {
        public UseTheWebsiteToPurchaseFood() : base() { }

        [Fact]
        public void New_customer_purchase()
        {
            var newUser = User.GetNew();
            var area = "AR51 1AA";
            this.Given(step => _I_have_made_an_order_for_the_meal(area), includeInputsInStepTitle: false)
                .When(step => _I_create_an_account_with(newUser.SignUpDetails))
                .And( step => _I_confirm_the_purchase_with_cash(newUser.Address))
                .Then(step => _I_see_a_confirmation_of_my_order())
                .BDDfy(); 
        }

        private void _I_see_a_confirmation_of_my_order()
        {
            Should.NotThrow( () => _orderConfirmationPage);
        }

        [StepTitle(" I confirm the purchase with cash", false)]
        private void _I_confirm_the_purchase_with_cash(Address address)
        {
            _deliveryDetailsPage.Phone(address.Phone)
                                .Address1(address.Address1)
                                .City(address.City)
                                .Submit();

            _deliveryTimeConfirmationPage.Submit();

            _orderPaymentPage.SelectPayWithCash()
                             .PlaceOrder();
        }
        
    }
}
