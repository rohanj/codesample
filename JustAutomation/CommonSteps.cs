﻿using JustAutomation.DataGenerator;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.BDDfy;

namespace JustAutomation
{
    public abstract class CommonSteps : TestBase
    {
        public void _I_have_made_an_order_for_the_meal(string area)
        {
            I_want_food_in(area);
            I_search_for_restaurants(area);
            _searchResultsPage.ChooseARestaurant();
            _restaurantPage.CreateAnOrder()
                           .GoToCheckout();
        }

        public void I_search_for_restaurants(string area)
        {
            _homepage.Search(area);
        }

        public void I_want_food_in(string area)
        {
            // the only value this function provides is a method name that will be used
            // in the report
        }

        // Assertions
        public void I_should_see_some_restaurants_in(string area)
        {
            var expectedResults = DataGenerator.Restaurant.GetExpected(area: area);
            var actualResults = _searchResultsPage.GetRestaurantNames();

            foreach (var result in expectedResults)
            {
                actualResults.ShouldContain(result);
            }
        }

        [StepTitle(" I create an account with the details: [{0}]", false)]
        public void _I_create_an_account_with(SignUpDetails newUser)
        {
            _signInPage.GoToSignUpPage()
                       .SignUp(newUser);
        }
    }
}
