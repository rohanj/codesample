﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestStack.BDDfy.Reporters.Html;

namespace JustAutomation
{
    public class JustEatReportConfig : DefaultHtmlReportConfiguration
    {
        public override string OutputFileName
        {
            get { return "RohanJanjuaTechTestReport.html"; }
        }

        public override string ReportHeader
        {
            get { return "Rohan Janjua Tech Test"; }
        }

        public override string ReportDescription
        {
            get { return "Two tests should pass, one should fail"; }
        }

        /// Embed jQuery in the report so people can see it with no internet connectivity
        public override bool ResolveJqueryFromCdn
        {
            get { return false; }
        }
    }
}
