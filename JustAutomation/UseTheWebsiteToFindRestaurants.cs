﻿using System;
using Xunit;
using TestStack.BDDfy;
using JustAutomation.Pages;
using Shouldly;
using System.Collections.Generic;
using JustAutomation.Pages.Web;

namespace JustAutomation
{
    [Story(
        AsA = "As a hungry customer",
        IWant = "I want to be able to find restaurants in my area",
        SoThat = "So that I can order food")]
    public class UseTheWebsiteToFindRestaurants : CommonSteps
    {
        public UseTheWebsiteToFindRestaurants() : base()
        {}

        [Fact]
        public void Search_for_restaurants_in_an_area()
        {
            var area = "AR51 1AA";

            this.Given(step => I_want_food_in(area))
                .When(step => I_search_for_restaurants(area), includeInputsInStepTitle: false)
                .Then(step => I_should_see_some_restaurants_in(area))
                .BDDfy();
        }

    }
}
