## Story: Use the website to find restaurants
 **As a hungry customer**  
 **I want to be able to find restaurants in my area**  
 **So that I can order food**  

### Search for restaurants in an area
  Given i want food in AR51 1AA  
  When i search for restaurants  
  Then i should see some restaurants in AR51 1AA  

## Story: Use the website to purchase food
 **As a hungry customer**  
 **I want to order some nourishing food**  
 **So that I can satisfy my hunger**  

### New customer purchase
  Given  I have made an order for the meal  
  When  I create an account with the details : [name : test pyoiaisbos , email : test .automation .box +pyoiaisbos @gmail .com , password : automation ]  
  And  I confirm the purchase with cash  
  Then  I see a confirmation of my order  

## Story: Website is safe from automation bots
 **As a Restaurant Owner**  
 **I want to make sure that only humans are ordering food**  
 **So that I only make food for real customers**  

### Prevent bots from creating accounts
  Given  I have made an order for the meal  
  When  I create an account with the details : [name : test fxjbvdgjf , email : test .automation .box +fxjbvdgjf @gmail .com , password : automation ]  
  Then  I am required to prove that I am human :  

